package model;
import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import constant.Constant;

class StationBasedFareCalculationTest {

	@BeforeEach
	void setUp() throws Exception {
	}

	@Test
	void testCalculateFare() throws SQLException {
		float result = Constant.BASE_FARE + Constant.ADDITIONAL_FARE;
		
		assertEquals(result, StationBasedFareCalculation.calculateFare(2, 4));
	}

}
