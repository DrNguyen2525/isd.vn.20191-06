package main;
import java.sql.SQLException;

import hust.soict.se.customexception.InvalidIDException;
import view.UserInterface;

public class Main {

	/**
	 * @param args
	 * @throws SQLException if there was a problem querying the database
	 * @throws InvalidIDException when the barcode is invalid
	 */
	public static void main(String[] args) throws SQLException, InvalidIDException {
		UserInterface UI = UserInterface.getInstance();
		UI.showMenu();
	}

}
