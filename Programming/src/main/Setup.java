package main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import constant.Constant;
import hust.soict.se.gate.Gate;
import hust.soict.se.recognizer.TicketRecognizer;
import hust.soict.se.scanner.CardScanner;

public class Setup {

	public static CardScanner cardScanner = CardScanner.getInstance();

	public static TicketRecognizer ticketRecognizer = TicketRecognizer.getInstance();

	public static Gate gate = Gate.getInstance();

	/**
	 * @return the connection to Database
	 */
	public static Connection connectToDatabase() {
		Connection conn = null;
		try {
			String url = "jdbc:" + Constant.DB_CONNECTION + "://" + Constant.DB_HOST + ":" + Constant.DB_PORT + "/"
					+ Constant.DB_DATABASE;
			conn = DriverManager.getConnection(url, Constant.DB_USERNAME, Constant.DB_PASSWORD);

			// System.out.println("Got it !");

		} catch (SQLException e) {
			throw new Error("Problem", e);
		}
		return conn;
	}

	/**
	 * @param conn the connection to Database
	 * @throws SQLException
	 */
	public static void closeConnectionToDatabase(Connection conn) throws SQLException {
		conn.close();
	}

	/**
	 * @param conn  the connection to Database
	 * @param query SQL query
	 * @return the result set
	 * @throws SQLException
	 */
	public static ResultSet executeQuery(Connection conn, String query) throws SQLException {
		Statement stmt;
		ResultSet rs;

		try {
			stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
		} catch (SQLException e) {
			throw new Error("Problem", e);
		} finally {
			// if (stmt != null) {
			// stmt.close();
			// }
		}
		return rs;
	}

	/**
	 * @param conn  the connect to Database
	 * @param query the SQL query
	 * @return the number of rows affected
	 * @throws SQLException
	 */
	public static int executeUpdate(Connection conn, String query) throws SQLException {
		Statement stmt;
		int rows_affected;

		try {
			stmt = conn.createStatement();
			rows_affected = stmt.executeUpdate(query);
		} catch (SQLException e) {
			throw new Error("Problem", e);
		} finally {
			// if (stmt != null) {
			// stmt.close();
			// }
		}
		return rows_affected;
	}

}
