package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import constant.Constant;
import main.Setup;
import view.UserInterface;

public class OneWayTicket extends TravelPermit {

	private float fare;

	private int status;

	private int registeredEmbarkationId;

	private int registeredDisembarkationId;

	private int embarkationId;

	public OneWayTicket(String ticketId, float baseFare, String lastAction, float ticketFare, int status,
			int registeredEmbarkationId, int registeredDisembarkationId, int embarkationId) {
		super(ticketId, baseFare, lastAction);
		this.fare = ticketFare;
		this.status = status;
		this.registeredEmbarkationId = registeredEmbarkationId;
		this.registeredDisembarkationId = registeredDisembarkationId;
		this.embarkationId = embarkationId;
	}

	/**
	 * Get a one-way ticket fare specified by ID from database
	 * 
	 * @param id the one-way ticket ID
	 * @return the one-way ticket fare
	 * @throws SQLException if there was a problem querying the database
	 */
	public static float getFareById(String id) throws SQLException {
		Connection conn = Setup.connectToDatabase();
		ResultSet rs = Setup.executeQuery(conn, "SELECT fare FROM one_way_tickets WHERE id = \"" + id + "\"");
		rs.next();
		float fare = rs.getFloat("fare");
		Setup.closeConnectionToDatabase(conn);
		return fare;
	}

	/**
	 * Get a one-way ticket status specified by ID from database
	 * 
	 * @param id the one-way ticket ID
	 * @return the one-way ticket status
	 * @throws SQLException if there was a problem querying the database
	 */
	public static int getStatusById(String id) throws SQLException {
		Connection conn = Setup.connectToDatabase();
		ResultSet rs = Setup.executeQuery(conn, "SELECT status FROM one_way_tickets WHERE id = \"" + id + "\"");
		rs.next();
		int status = rs.getInt("status");
		Setup.closeConnectionToDatabase(conn);
		return status;
	}

	/**
	 * Get the registered embarkation ID printed on a one-way ticket specified by ID
	 * from database
	 * 
	 * @param id the one-way ticket ID
	 * @return the embarkation ID registered by passenger
	 * @throws SQLException if there was a problem querying the database
	 */
	public static int getRegisteredEmbarkationIdById(String id) throws SQLException {
		Connection conn = Setup.connectToDatabase();
		ResultSet rs = Setup.executeQuery(conn,
				"SELECT registered_embarkation_id FROM one_way_tickets WHERE id = \"" + id + "\"");
		rs.next();
		int registeredEmbarkationId = rs.getInt("registered_embarkation_id");
		Setup.closeConnectionToDatabase(conn);
		return registeredEmbarkationId;
	}

	/**
	 * Get the registered disembarkation ID printed on a one-way ticket specified by
	 * ID from database
	 * 
	 * @param id the one-way ticket ID
	 * @return the disembarkation ID registered by passenger
	 * @throws SQLException if there was a problem querying the database
	 */
	public static int getRegisteredDisembarkationIdById(String id) throws SQLException {
		Connection conn = Setup.connectToDatabase();
		ResultSet rs = Setup.executeQuery(conn,
				"SELECT registered_disembarkation_id FROM one_way_tickets WHERE id = \"" + id + "\"");
		rs.next();
		int registeredDisembarkationId = rs.getInt("registered_disembarkation_id");
		Setup.closeConnectionToDatabase(conn);
		return registeredDisembarkationId;
	}

	/**
	 * Get the station ID that the passenger actually embarked using the one-way
	 * ticket specified by ID from database
	 * 
	 * @param id the one-way ticket ID
	 * @return the actual embarkation ID of passenger
	 * @throws SQLException if there was a problem querying the database
	 */
	public static int getEmbarkationIdById(String id) throws SQLException {
		Connection conn = Setup.connectToDatabase();
		ResultSet rs = Setup.executeQuery(conn, "SELECT embarkation_id FROM one_way_tickets WHERE id = \"" + id + "\"");
		rs.next();
		int embarkationId = rs.getInt("embarkation_id");
		Setup.closeConnectionToDatabase(conn);
		return embarkationId;
	}

	/**
	 * Save this one-way ticket information to database
	 * 
	 * @see model.TravelPermit#saveToDb()
	 */
	@Override
	protected void saveToDb() throws SQLException {
		Connection conn = Setup.connectToDatabase();
		Setup.executeUpdate(conn,
				"UPDATE travel_permits SET last_action = \"" + this.lastAction + "\" WHERE id = \"" + this.id + "\"");
		Setup.executeUpdate(conn, "UPDATE one_way_tickets SET status = " + this.status + ", embarkation_id = "
				+ this.embarkationId + " WHERE id = \"" + this.id + "\"");
		Setup.closeConnectionToDatabase(conn);
	}

	/**
	 * @param embarkationId the actual embarkation ID of passenger
	 * @return true if the passenger can enter the platform area at the station
	 *         specified by the given ID with this one-way ticket, false otherwise
	 * @see model.TravelPermit#enterStation(int)
	 */
	@Override
	public boolean enterStation(int embarkationId) throws SQLException {
		if (status == 0) {
			UserInterface.setMessage("The ticket is already used");
			return false;
		}

		if (Station.getDistanceById(embarkationId) >= Station.getDistanceById(this.registeredEmbarkationId)
				&& Station.getDistanceById(embarkationId) <= Station.getDistanceById(this.registeredDisembarkationId)) {
			this.lastAction = "enter";
			this.embarkationId = embarkationId;
			this.status = 0;
			this.saveToDb();
			return true;
		}

		UserInterface.setMessage("Ticket can only be used enter a station in between "
				+ Station.getNameById(this.registeredEmbarkationId) + " station and "
				+ Station.getNameById(this.registeredDisembarkationId) + " station");
		return false;
	}

	/**
	 * @param disembarkationId the actual disembarkation ID of passenger
	 * @return true if the passenger can exit the platform area at the station
	 *         specified by the given ID with this one-way ticket, false otherwise
	 * @see model.TravelPermit#exitStation(int)
	 */
	@Override
	public boolean exitStation(int disembarkationId) throws SQLException {
		if (this.lastAction == null || this.lastAction.equals("exit")) {
			UserInterface.setMessage("The ticket is not used to enter a station before");
			return false;
		}

		float fare = StationBasedFareCalculation.calculateFare(this.embarkationId, disembarkationId);
		if (this.fare < fare) {
			UserInterface.setMessage("Not enough balance: Expected " + fare + " " + Constant.CURRENCY);
			return false;
		}

		this.lastAction = "exit";
		this.saveToDb();
		return true;
	}

}
