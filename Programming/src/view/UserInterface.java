package view;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Scanner;

import controller.Controller;
import hust.soict.se.customexception.InvalidIDException;
import main.Setup;

public class UserInterface {

    private static UserInterface instance = new UserInterface();

    /**
     * @return the instance of class UserInterface
     */
    public static UserInterface getInstance() {
        return instance;
    }

    private UserInterface() {

    }

    private static String travelPermitType;

    private static String travelPermitId;

    private static String travelPermitInfo;

    private static String message;

    /**
     * @param travelPermitType the type of Travel Permit
     */
    public static void setTravelPermitType(String travelPermitType) {
        UserInterface.travelPermitType = travelPermitType;
    }

    /**
     * @param travelPermitId the type of Travel Permit
     */
    public static void setTravelPermitId(String travelPermitId) {
        UserInterface.travelPermitId = travelPermitId;
    }

    /**
     * @param travelPermitInfo the info of Travel Permit
     */
    public static void setTravelPermitInfo(String travelPermitInfo) {
        UserInterface.travelPermitInfo = travelPermitInfo;
    }

    /**
     * @param message the message to display
     */
    public static void setMessage(String message) {
        UserInterface.message = message;
    }

    /**
     * Show the Success Message
     */
    private void showSuccessMessage() {
        System.out.println("Welcome");
    }

    /**
     * Show the Error Message
     */
    private void showErrorMessage() {
        System.out.println("\nInvalid " + travelPermitType);
        System.out.println("\n" + message);
    }

    /**
     * Show the info of Travel Permit
     */
    public static void showTravelPermitInfo() {
        System.out.print("\nType: " + travelPermitType);
        System.out.println("\tID: " + travelPermitId);
        System.out.println("\n" + travelPermitInfo);
        System.out.println("");
    }

    /**
     * @throws SQLException
     * @throws InvalidIDException
     */
    public void showMenu() throws SQLException, InvalidIDException {
        Scanner input = new Scanner(System.in);
        HashMap<Integer, String> barcodes = new HashMap<Integer, String>();
        Controller controller = Controller.getInstance();
        Connection conn;
        ResultSet rs;
        int action = 0, stationId = 0, barcode = 0;
        String name;
        Boolean invalid;

        while (true) {
            System.out.println("\nAutomated Fare Controller System");
            System.out.println("---------------------------------");
            System.out.println("These are stations in the line M14 of Paris:\n");
            conn = Setup.connectToDatabase();

            rs = Setup.executeQuery(conn, "SELECT id, name FROM stations");
            while (rs.next()) {
                stationId = rs.getInt("id");
                name = rs.getString("name");
                System.out.println(stationId + ". " + name);
            }

            System.out.println("\nAvailable actions: 1-enter station, 2-exit station, 0-quit program");
            do {
                System.out.print("Option: ");
                try {
                    action = input.nextInt();
                    if (action != 1 && action != 2 && action != 0) {
                        invalid = true;
                    } else
                        invalid = false;
                } catch (InputMismatchException e) {
                    input.next();
                    invalid = true;
                }
            } while (invalid);

            if (action == 0) {
                System.out.println("\nGood bye.");
                break;
            }

            do {
                System.out.print("Station: ");
                try {
                    stationId = input.nextInt();
                    if (stationId < 1 || stationId > 9) {
                        invalid = true;
                    } else
                        invalid = false;
                } catch (InputMismatchException e) {
                    input.next();
                    invalid = true;
                }
            } while (invalid);

            System.out.println("\nAction: " + action + "\nStation ID: " + stationId);

            System.out.println("\nThese are existing tickets/cards:\n");
            barcodes.put(1, "abcdefgh");
            barcodes.put(2, "ijklmnop");
            barcodes.put(3, "ABCDEFGH");
            barcodes.put(4, "ponmlkji");

            for (Integer i : barcodes.keySet())
                System.out.println(i + ". " + barcodes.get(i));

            do {
                System.out.print("\nPlease choose one: ");
                try {
                    barcode = input.nextInt();
                    if (barcode < 1 || barcode > 4) {
                        invalid = true;
                    } else
                        invalid = false;
                } catch (InputMismatchException e) {
                    input.next();
                    invalid = true;
                }
            } while (invalid);

            switch (action) {
            case 1:
                if (controller.enterStation(barcodes.get(barcode), stationId))
                    this.showSuccessMessage();
                else
                    this.showErrorMessage();
                break;

            case 2:
                if (controller.exitStation(barcodes.get(barcode), stationId))
                    this.showSuccessMessage();
                else
                    this.showErrorMessage();
                break;
            }

            System.out.print("\nPress Enter to continue...");
            input.nextLine();
            input.nextLine();
        }

        input.close();
    }

}