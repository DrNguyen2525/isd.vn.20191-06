package model;

import java.sql.SQLException;

import constant.Constant;

public class StationBasedFareCalculation {

	/**
	 * @param embarkationId    the ID of embarkation station
	 * @param disembarkationId the ID of disembarkation station
	 * @return the fare between 2 station
	 * @throws SQLException
	 */
	public static float calculateFare(int embarkationId, int disembarkationId) throws SQLException {
		float distance = Math.abs(Station.getDistanceById(disembarkationId) - Station.getDistanceById(embarkationId));

		if (distance <= Constant.BASE_DISTANCE)
			return Constant.BASE_FARE;
		return Constant.BASE_FARE
				+ (float) Math.ceil((distance - Constant.BASE_DISTANCE) / Constant.ADDITIONAL_DISTANCE)
						* Constant.ADDITIONAL_FARE;
	}

}
