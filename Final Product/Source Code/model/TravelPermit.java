package model;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

import main.Setup;

public abstract class TravelPermit {

	protected String id;

	protected float baseFare;

	protected String lastAction;

	public TravelPermit(String id, float baseFare, String lastAction) {
		super();
		this.id = id;
		this.baseFare = baseFare;
		this.lastAction = lastAction;
	}

	/**
	 * Get the travel permit ID specified by code from database
	 * 
	 * @param code travel permit code processed by Ticket Recognizer or Card Scanner
	 * @return the travel permit ID
	 */
	public static String getIdByCode(String code) throws SQLException {
		Connection conn = Setup.connectToDatabase();
		ResultSet rs = Setup.executeQuery(conn, "SELECT id FROM travel_permits WHERE code = \"" + code + "\"");
		rs.next();
		String id = rs.getString("id");
		Setup.closeConnectionToDatabase(conn);
		return id;
	}

	/**
	 * Get the travel permit base fare specified by ID from database
	 * 
	 * @param id the travel permit ID
	 * @return the travel permit base fare
	 * @throws SQLException
	 */
	public static float getBaseFareById(String id) throws SQLException {
		Connection conn = Setup.connectToDatabase();
		ResultSet rs = Setup.executeQuery(conn, "SELECT base_fare FROM travel_permits WHERE id = \"" + id + "\"");
		rs.next();
		float baseFare = rs.getFloat("base_fare");
		Setup.closeConnectionToDatabase(conn);
		return baseFare;
	}

	/**
	 * Get the travel permit last action specified by ID from database
	 * 
	 * @param id the travel permit ID
	 * @return the travel permit last action
	 * @throws SQLException
	 */
	public static String getLastActionById(String id) throws SQLException {
		Connection conn = Setup.connectToDatabase();
		ResultSet rs = Setup.executeQuery(conn, "SELECT last_action FROM travel_permits WHERE id = \"" + id + "\"");
		rs.next();
		String lastAction = rs.getString("last_action");
		Setup.closeConnectionToDatabase(conn);
		return lastAction;
	}

	/**
	 * @param embarkationId the embarkation ID of passenger
	 * @return true if the passenger can enter the platform area at the station
	 *         specified by the given ID with this travel permit, false otherwise
	 * @throws SQLException if there was a problem querying the database
	 */
	public abstract boolean enterStation(int embarkationId) throws SQLException;

	/**
	 * @param disembarkationId the disembarkation ID of passenger
	 * @return true if the passenger can exit the platform area at the station
	 *         specified by the given ID with this travel permit, false otherwise
	 * @throws SQLException if there was a problem querying the database
	 */
	public abstract boolean exitStation(int disembarkationId) throws SQLException;

	/**
	 * Save this travel permit information to database
	 * 
	 * @throws SQLException if there was a problem querying the database
	 */
	protected abstract void saveToDb() throws SQLException;

}
