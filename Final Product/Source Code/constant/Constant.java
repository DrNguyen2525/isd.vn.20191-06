package constant;

public class Constant {

	public static final String DB_CONNECTION = "mysql";		
			
	public static final String DB_HOST = "localhost";
	
	public static final String DB_PORT = "3306";
	
	public static final String DB_DATABASE = "AFC";
	
	public static final String DB_USERNAME = "root";
	
	public static final String DB_PASSWORD = "123456";

	public static final int PREFIX_LENGTH = 2;
	
	public static final String ONE_WAY_TICKET_PREFIX = "OW";
	
	public static final String TWENTY_FOUR_HOUR_TICKET_PREFIX = "TF";
	
	public static final String PREPAID_CARD_PREFIX = "PC";
	
	public static final float BASE_DISTANCE = 5;
	
	public static final float ADDITIONAL_DISTANCE = 2;
	
	public static final float BASE_FARE = 1.9f;
	
	public static final float ADDITIONAL_FARE = 0.4f;
	
	public static final String CURRENCY = "euros";

}
