Doer: Vu Khanh Ly
Reviewer: Nguyen Dich Long

--------------------------------------------

1. Task: 
- Do communication diagram and class diagram for use cases:
  + Enter platform area with one-way ticket
  + Exit platform area with one-way ticket

2. Review:
- Errors: In Enter/Exit Platform Area With Oneway Ticket Class Diagram method can not be private
- Comments: None
- Proposals for Corrections: Change those methods' visibility to public

