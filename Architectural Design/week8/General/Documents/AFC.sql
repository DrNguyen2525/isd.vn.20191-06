DROP SCHEMA

IF EXISTS AFC;
	CREATE SCHEMA AFC COLLATE = utf8_general_ci;

USE AFC;

--
-- Table structure for table `stations`
--

CREATE TABLE stations (
  id INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(45) NULL,
  distance FLOAT NULL,
  PRIMARY KEY (id));

--
-- Table structure for table `travel_permits`
--

CREATE TABLE travel_permits (
  id VARCHAR(14) NOT NULL,
  code VARCHAR(16) NOT NULL UNIQUE,
  base_fare FLOAT NULL,
  last_action VARCHAR(5) NULL,
  PRIMARY KEY (id));

--
-- Table structure for table `one_way_tickets`
--

CREATE TABLE one_way_tickets (
  id VARCHAR(14) NOT NULL,
  fare FLOAT NULL,
  status INT NULL,
  registered_embarkation_id INT NULL,
  registered_disembarkation_id INT NULL,
  embarkation_id INT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (id) REFERENCES travel_permits(id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (registered_embarkation_id) REFERENCES stations(id) ON DELETE SET NULL ON UPDATE CASCADE,
  FOREIGN KEY (registered_disembarkation_id) REFERENCES stations(id) ON DELETE SET NULL ON UPDATE CASCADE,
  FOREIGN KEY (embarkation_id) REFERENCES stations(id) ON DELETE SET NULL ON UPDATE CASCADE);

--
-- Table structure for table `24_hour_tickets`
--

CREATE TABLE 24_hour_tickets (
  id VARCHAR(14) NOT NULL,
  first_used_time DATETIME NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (id) REFERENCES travel_permits(id) ON DELETE CASCADE ON UPDATE CASCADE);

--
-- Table structure for table `prepaid_cards`
--

CREATE TABLE prepaid_cards (
  id VARCHAR(14) NOT NULL,
  balance FLOAT NULL,
  embarkation_id INT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (id) REFERENCES travel_permits(id) ON DELETE CASCADE ON UPDATE CASCADE,
  FOREIGN KEY (embarkation_id) REFERENCES stations(id) ON DELETE SET NULL ON UPDATE CASCADE);

--
-- Dumping data for table `stations`
--

INSERT INTO stations (name, distance) VALUES
('Saint-Lazare', 0),
('Madeleine', 5),
('Pyramides', 8.5),
('Chatelet', 11.3),
('Gare de Lyon', 15.8),
('Bercy', 18.9),
('Cour Saint-Emilion', 22),
('Bibliotheque Francois Mitterrand', 25.3),
('Olympiades', 28.8);

--
-- Dumping data for table `travel_permits`
--

INSERT INTO travel_permits (id, code, base_fare, last_action) VALUES
('OW201917110000', 'e8dc4081b13434b4', 1.9, NULL),
('TF201917110000', '07c84c6c4ba59f88', 8.5, NULL),
('TF201917110001', 'bab1246b02772bb0', 8.5, 'exit'),
('PC201917110000', '9ac2197d9258257b', 18.5, NULL);

--
-- Dumping data for table `one_way_tickets`
--

INSERT INTO one_way_tickets (id, fare, status, registered_embarkation_id, registered_disembarkation_id) VALUES
('OW201917110000', 3.42, 1, 4, 9);

--
-- Dumping data for table `24_hour_tickets`
--

INSERT INTO 24_hour_tickets (id, first_used_time) VALUES
('TF201917110000', NULL),
('TF201917110001', '2019-05-15 14:35:00');

--
-- Dumping data for table `prepaid_cards`
--

INSERT INTO prepaid_cards (id, balance) VALUES
('PC201917110000', 5.65);

