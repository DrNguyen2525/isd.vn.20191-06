# Homework 6 - Design Class and Package
#### GROUP 6

## Description
Automated fare collection (AFC) system
- Identify Design Classes for all Analysis Classes
- Group Design Classes in Packages

## Members' tasks
1. Nguyen Dich Long
- Identify Design Classes for Analysis Classes:
  + PrepaidCard
  + EnterPlatformController
- Group Design Classes in Packages

2. Vu Khanh Ly
- Identify Design Classes for Analysis Classes:
  + OnewayTicket
  + InformationSendingInterface
  + GateControlInterface

3. Nguyen Huu Manh
- Identify Design Classes for Analysis Classes:
  + 24HourTicket
  + ExitPlatformController
  + InsertionInterface

## Review
| Doer | Reviewer | Status (%) |
| --- | --- | --- |
| Nguyen Dich Long | Vu Khanh Ly | 100 |
| Vu Khanh Ly | Nguyen Huu Manh | 100 |
| Nguyen Huu Manh | Nguyen Dich Long | 100 |

